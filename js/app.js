$(document).ready(function() {
    $(function() {
        new WOW().init();
    });
    function burgerMenu(selector) {
        let menu = $(selector);
        let button = menu.find('.burger-menu_button', '.burger-menu_lines');
        let links = menu.find('.link');
        let overlay = menu.find('.burger-menu_overlay');

        button.on('click', (e) => {
            e.preventDefault();
            toggleMenu();
        });

        links.on('click', () => toggleMenu());
        overlay.on('click', () => toggleMenu());

        function toggleMenu() {
            menu.toggleClass('burger-menu_active');
            button.toggleClass('burger-menu_button_active')
            if (menu.hasClass('burger-menu_active')) {
                $('body').css('overlow', 'hidden');
            } else {
                $('body').css('overlow', 'visible');
            }
        }
    }

    burgerMenu('.burger-menu');

    $('#btnModal').on('click', () => {
        $('.modal').css('display', 'flex');
    })
    $('.exit').on('click', () => {
        $('.modal').css('display', 'none');
    })

    function sendFeedback(feedbackData){
        $.ajax({
            url: 'https://60df103aabbdd9001722d215.mockapi.io/api/v1/test',
            method: 'post',
            dataType: 'html',
            data:feedbackData,
            success: function(){
                 alert('good');
            }
        });
    }
    $('#btnReserve').on('click', () => {
        let feedbackData = {
            reserveName:$('#reserveName').val(),
            contactData: $('#reserveNum').val(),
            reserveText: $('#reserveText').val()
        }
        if($('.inp').val().length === 0 ) {
            return false
        } else {
            $('.modal').css('display', 'none');
            sendFeedback(feedbackData);
            $('.inp').val('')
        }
        
    })
    $('#btnRequest').on('click', () => {
        let feedbackData = {
            reserveName:$('#requestName').val(),
            contactData: $('#requestEmail').val(),
            reserveText: $('#requestText').val()
        }
        if($('.inp2').val().length === 0 ) {
            return false
        }
        sendFeedback(feedbackData);
        $('.inp2').val('')
    })
    
    $('.head').click(function(){
        $(this).toggleClass('active');
        $(this).parent().find('.arrow').toggleClass('arrow-animate');
        $(this).parent().find('.content').slideToggle(280);
      });

})